# Calculadora e Questões práticas

3. **Este código soma os dois primeiros argumentos digitados, e para roda-lo, basta digitar no seu terminal "node calculadora.js "argumento 1" "argumento 2".**

4. **Este código deve ter o commit "feat", que indica que alguma coisa nova foi adicionada ao código.**

5. **Foi adicionado uma função ao código e para realizar o conventional commit usa-se o "perf commit", que indica uma otimização do código.**

6. **Os erros eram o arg[0] no default e o valor dos args das funções, que deveriam ser respectivamente [1] e [2] e para realizar seu commit deve-se usar o "fix commit", que indica uma correção.**

7. **Foi criada uma branch usando o comando "git branch "nome"" e foi cricado a divisão dentro dessa branch.**

8. **Foi efetuado um merge request no gitLab, agora as alterações estão na branch principal.**

9. **Para reverter o commit foi usado o "git revert "codigo do commit", assim voltando para o commit anterior.**

10. **No código do Jõao estava faltando o "const args = process.argv.slice(2);" para dar funcionalidade ao args. Agora com o args declarado, para utilizá-lo basta abrir o seu terminal e digitar "node calculadora.js "numero" "operador" "numero".**
 
